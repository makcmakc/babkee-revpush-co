import { createRouter, createWebHistory } from 'vue-router'

// App Layout
import AppLayout from '../layouts/index.vue'

// Components

export const router = createRouter({
  history: createWebHistory(),
  strict: true,
  scrollBehavior: () => ({ left: 0, top: 0 }),
  routes: [
    {
      path: '/',
      component: AppLayout,
      redirect: '/overview',
      children: [
        {
          path: 'overview',
          name: 'overview',
          component: () => import('../views/reports/overview/index.vue'),
          meta: { title: 'overview', icon: 'overview', affix: true },
        },
      ],
    },
    {
      path: '/adgroup',
      component: AppLayout,
      children: [
        {
          path: '/adgroup',
          name: 'adgroup',
          component: () => import('../views/reports/adgroup/index.vue'),
          meta: { title: 'adgroup', icon: 'adgroup', affix: true },
        },
      ],
    },
    {
      path: '/users',
      component: AppLayout,
      children: [
        {
          path: '/users',
          name: 'users',
          component: () => import('../views/reports/users/index.vue'),
          meta: { title: 'users', icon: 'users', affix: true },
        },
      ],
    },
    {
      path: '/sites',
      component: AppLayout,
      children: [
        {
          path: '/sites',
          name: 'sites',
          component: () => import('../views/reports/sites/index.vue'),
          meta: { title: 'sites', icon: 'sites', affix: true },
        },
      ],
    },
    {
      path: '/charts',
      component: AppLayout,
      children: [
        {
          path: '/charts',
          name: 'charts',
          component: () => import('../views/reports/charts/index.vue'),
          meta: { title: 'charts', icon: 'charts', affix: true },
        },
      ],
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/sys/login/Login.vue'),
    },
    {
      path: '/profile',
      component: AppLayout,
      children: [
        {
          path: '/profile',
          name: 'profile',
          component: () => import('../views/sys/profile/index.vue'),
          meta: { title: 'profile', icon: 'profile', affix: true },
        },
      ],
    },
  ],
})

export function resetRouter() {
  location.reload()
}

export function setupRouter(app) {
  app.use(router)
}
