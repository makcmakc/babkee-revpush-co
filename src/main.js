// Register WindiCSS styles
import 'virtual:windi-base.css'
import 'virtual:windi-components.css'
import 'virtual:windi-utilities.css'

// Register own styles
import './styles/main.scss'

// Element Plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import { createApp } from 'vue'
import { setupRouter } from './router'

import App from './App.vue'

function init() {
  const app = createApp(App)

  // Register Element Plus
  app.use(ElementPlus)

  // Configure routing
  setupRouter(app)

  app.mount('#app')
}

init()
