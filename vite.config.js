import { defineConfig, loadEnv } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import { VitePWA } from 'vite-plugin-pwa'

function pathResolve(dir) {
  return resolve(process.cwd(), '.', dir)
}

export default defineConfig((command, mode) => {
  const root = process.cwd()
  const { VITE_PORT } = loadEnv(mode, root)

  return {
    base: '/',
    server: {
      // Listening on all local IPs
      host: true,
      port: VITE_PORT,
    },
    resolve: {
      alias: [
        {
          find: /\/@\//,
          replacement: pathResolve('src') + '/',
        },
      ],
    },
    plugins: [
      WindiCSS({
        scan: {
          dirs: ['.'], // all files in the cwd
          fileExtensions: ['vue', 'js', 'ts'], // also enabled scanning for js/ts
        },
      }),
      VitePWA({
        outDir: './dist',
        registerType: 'prompt',
        ManifestIcons: false,
        manifest: {
          id: '/',
          name: 'Vite Plugin PWA',
          short_name: 'PWA for Vite',
          description: 'Zero-config PWA for Vite',
          theme_color: '#ffffff',
        },
        workbox: {
          globPatterns: ['**/*.{css,js,html,svg,png,ico,txt,woff2}'],
          runtimeCaching: [
            {
              urlPattern: /^https:\/\/fonts\.googleapis\.com\/.*/i,
              handler: 'CacheFirst',
              options: {
                cacheName: 'google-fonts-cache',
                expiration: {
                  maxEntries: 10,
                  maxAgeSeconds: 60 * 60 * 24 * 365, // <== 365 days
                },
                cacheableResponse: {
                  statuses: [0, 200],
                },
              },
            },
            {
              urlPattern: /^https:\/\/fonts\.gstatic\.com\/.*/i,
              handler: 'CacheFirst',
              options: {
                cacheName: 'gstatic-fonts-cache',
                expiration: {
                  maxEntries: 10,
                  maxAgeSeconds: 60 * 60 * 24 * 365, // <== 365 days
                },
                cacheableResponse: {
                  statuses: [0, 200],
                },
              },
            },
            {
              urlPattern: /^https:\/\/cdn\.jsdelivr\.net\/.*/i,
              handler: 'NetworkFirst',
              options: {
                cacheName: 'jsdelivr-images-cache',
                expiration: {
                  maxEntries: 10,
                  maxAgeSeconds: 60 * 60 * 24 * 7, // <== 7 days
                },
                cacheableResponse: {
                  statuses: [0, 200],
                },
              },
            },
          ],
        },
      }),
      vue(),
    ],
  }
})
